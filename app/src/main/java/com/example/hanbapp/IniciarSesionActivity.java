package com.example.hanbapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hanbapp.Common.Common;
import com.example.hanbapp.Model.Usuarios;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class IniciarSesionActivity extends AppCompatActivity {
    EditText edtCel_signin, edtPass_signin;
    Button btnIniciarSesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion);

        edtPass_signin = (EditText) findViewById(R.id.edtPass_signin);
        edtCel_signin = (EditText) findViewById(R.id.edtCel_signin);
        btnIniciarSesion = (Button) findViewById(R.id.btnIniciarSesion);

        //Iniciar Firebase
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference tabla_usuarios = database.getReference("Usuarios"); //nombre de la "tabla" usuarios en Firebase

        btnIniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialogo de espera
                final ProgressDialog mDialog = new ProgressDialog(IniciarSesionActivity.this);
                mDialog.setMessage("Por favor, espere un momento...");
                mDialog.show();

                tabla_usuarios.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        // Revisa si existe el usuario en la Base de Datos
                        if (dataSnapshot.child(edtCel_signin.getText().toString()).exists()) {
                            //Obtener información del usuario
                            mDialog.dismiss();
                            Usuarios usuario = dataSnapshot.child(edtCel_signin.getText().toString()).getValue(Usuarios.class);
                            usuario.setTelefono(edtCel_signin.getText().toString()); // set Telefono
                            if (usuario.getPassword().equals(edtPass_signin.getText().toString())) {
                                Toast.makeText(IniciarSesionActivity.this, "Has iniciado sesión :)", Toast.LENGTH_SHORT).show();
                                Intent homeIntent = new Intent(IniciarSesionActivity.this, HomeActivity.class);
                                Common.currentUser = usuario;
                                startActivity(homeIntent);
                                finish();
                            } else {
                                Toast.makeText(IniciarSesionActivity.this, "Contraseña incorrecta", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            Toast.makeText(IniciarSesionActivity.this, "El usuario no existe o es incorrecto", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

    }
}
