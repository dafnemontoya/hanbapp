package com.example.hanbapp.EventBus;

import com.example.hanbapp.Database.ItemCarrito;

public class UpdateItemCarrito {
    private ItemCarrito itemCarrito;

    public UpdateItemCarrito(ItemCarrito itemCarrito) {
        this.itemCarrito = itemCarrito;
    }

    public ItemCarrito getItemCarrito() {
        return itemCarrito;
    }

    public void setItemCarrito(ItemCarrito itemCarrito) {
        this.itemCarrito = itemCarrito;
    }
}
