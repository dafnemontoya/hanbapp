package com.example.hanbapp.EventBus;

import com.example.hanbapp.Model.ModeloFood;

public class FoodItemClick {
    private boolean success;
    private ModeloFood modeloComidas;

    public FoodItemClick(boolean success, ModeloFood modeloComidas) {
        this.success = success;
        this.modeloComidas = modeloComidas;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ModeloFood getModeloComidas() {
        return modeloComidas;
    }

    public void setModeloComidas(ModeloFood modeloComidas) {
        this.modeloComidas = modeloComidas;
    }
}
