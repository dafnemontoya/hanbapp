package com.example.hanbapp.EventBus;

public class HideCarritoFAB {
    private boolean hidden;

    public HideCarritoFAB(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }
}
