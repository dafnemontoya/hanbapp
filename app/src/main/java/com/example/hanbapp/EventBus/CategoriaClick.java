package com.example.hanbapp.EventBus;

import com.example.hanbapp.Model.ModeloCategorias;

public class CategoriaClick {
    private boolean success;
    private ModeloCategorias modeloCategorias;

    public CategoriaClick(boolean success, ModeloCategorias modeloCategorias) {
        this.success = success;
        this.modeloCategorias = modeloCategorias;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ModeloCategorias getModeloCategorias() {
        return modeloCategorias;
    }

    public void setModeloCategorias(ModeloCategorias modeloCategorias) {
        this.modeloCategorias = modeloCategorias;
    }
}
