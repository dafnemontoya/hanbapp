package com.example.hanbapp.EventBus;

public class ContadorCarritoEvento {
    private boolean success;

    public ContadorCarritoEvento(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
