package com.example.hanbapp.ui.fooddetalle;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.andremion.counterfab.CounterFab;
import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.hanbapp.Common.Common;
import com.example.hanbapp.Database.CarritoDataSource;
import com.example.hanbapp.Database.CarritoDatabase;
import com.example.hanbapp.Database.CarritoLocalDataSource;
import com.example.hanbapp.Database.ItemCarrito;
import com.example.hanbapp.EventBus.ContadorCarritoEvento;
import com.example.hanbapp.Model.ModeloFood;
import com.example.hanbapp.Model.ModeloSize;
import com.example.hanbapp.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FoodDetalleFragment extends Fragment {
    private FoodDetalleViewModel foodDetalleViewModel;
    Unbinder unbinder;
    private CarritoDataSource carritoDataSource;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    @BindView(R.id.img_comida)
    ImageView img_comida;
    @BindView(R.id.btnCarrito)
    CounterFab btnCarrito;
    @BindView(R.id.btnRating)
    FloatingActionButton btnRating;
    @BindView(R.id.txt_nomComida)
    TextView txt_nomComida;
    @BindView(R.id.txt_descComida)
    TextView txt_descComida;
    @BindView(R.id.txt_precioComida)
    TextView txt_precioComida;
    @BindView(R.id.number_button)
    ElegantNumberButton number_button;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.radio)
    RadioGroup radio;

    @OnClick(R.id.btnRating)
    void onRatingButtonClick(){showDialogRating();}
    private void showDialogRating() {
    }

    //Al agregar al carrito...
    @OnClick(R.id.btnCarrito)
    void onAgregarItemCarrito() {
        ItemCarrito itemCarrito = new ItemCarrito();
        itemCarrito.setUid(Common.currentUser.getTelefono()); //recordar que es el teléfono

        itemCarrito.setIdComida(Common.comidaSelec.getId());
        itemCarrito.setNombreComida(Common.comidaSelec.getNombre());
        itemCarrito.setImagenComida(Common.comidaSelec.getImagen());
        itemCarrito.setCantidadComida(Integer.parseInt(number_button.getNumber()));
        itemCarrito.setPrecioComida(Double.valueOf(String.valueOf(Common.comidaSelec.getPrecio())));
        itemCarrito.setExtraPrecioComida(Common.calcularPrecioExtra(Common.comidaSelec.getUsuariosSelectedTamano(), Common.comidaSelec.getUsuariosSelectedComplementos()));
        if (Common.comidaSelec.getUsuariosSelectedComplementos() != null)
            itemCarrito.setComplementoComida(new Gson().toJson(Common.comidaSelec.getUsuariosSelectedComplementos()));
        else
            itemCarrito.setComplementoComida("Default");
        if (Common.comidaSelec.getUsuariosSelectedTamano() != null)
            itemCarrito.setTamanoComida(new Gson().toJson(Common.comidaSelec.getUsuariosSelectedTamano()));
        else
            itemCarrito.setTamanoComida("Default");

        carritoDataSource.getItemConTodasLasOpcionesEnCarrito(
                itemCarrito.getIdComida(),
                Common.currentUser.getTelefono(),
                itemCarrito.getTamanoComida(),
                itemCarrito.getComplementoComida())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ItemCarrito>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(ItemCarrito itemCarritoFromDB) {
                        if (itemCarritoFromDB.equals(itemCarrito)) {
                            //Ya en la DB, sólo hay que actualizar
                            itemCarritoFromDB.setExtraPrecioComida(itemCarrito.getExtraPrecioComida());
                            itemCarritoFromDB.setComplementoComida(itemCarrito.getComplementoComida());
                            itemCarritoFromDB.setTamanoComida(itemCarrito.getTamanoComida());
                            itemCarritoFromDB.setCantidadComida(itemCarritoFromDB.getCantidadComida() + itemCarrito.getCantidadComida());

                            carritoDataSource.updateItemCarrito(itemCarritoFromDB)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new SingleObserver<Integer>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onSuccess(Integer integer) {
                                            Toast.makeText(getContext(), "Se actualizó el carrito correctamente :)", Toast.LENGTH_SHORT).show();
                                            EventBus.getDefault().postSticky(new ContadorCarritoEvento(true));
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            Toast.makeText(getContext(), "(ACTUALIZAR CARRITO)"+e.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                        else {
                            //El item no estaba en el carrito antes, es decir, se inserta uno nuevo
                            compositeDisposable.add(carritoDataSource.insertOrReplaceAll(itemCarrito)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(()->{
                                        Toast.makeText(getContext(), "Se agregó al carrito existosamente :)", Toast.LENGTH_SHORT).show();
                                    }, throwable -> {
                                        Toast.makeText(getContext(), "(ERROR DEL CARRITO)"+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                    }));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e.getMessage().contains("empty")) {
                            //Default, si el carrito está vacío
                            compositeDisposable.add(carritoDataSource.insertOrReplaceAll(itemCarrito)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(()->{
                                        Toast.makeText(getContext(), "Se agregó al carrito existosamente :)", Toast.LENGTH_SHORT).show();
                                    }, throwable -> {
                                        Toast.makeText(getContext(), "(ERROR DEL CARRITO)"+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                    }));
                        }
                        else
                            Toast.makeText(getContext(), "(OBTENER CARRITO)"+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        foodDetalleViewModel =
                ViewModelProviders.of(this).get(FoodDetalleViewModel.class);
        View root = inflater.inflate(R.layout.fragment_food_detalle, container, false);
        unbinder = ButterKnife.bind(this, root);
        foodDetalleViewModel.getDetalleComidaList().observe(getViewLifecycleOwner(), modeloFood -> {
            mostrarInfo(modeloFood);
        });
        return root;
    }

    private void mostrarInfo(ModeloFood modeloFood) {
        carritoDataSource = new CarritoLocalDataSource(CarritoDatabase.getInstancia(getContext()).carritoDAO());

        Glide.with(getContext()).load(modeloFood.getImagen()).into(img_comida);
        txt_nomComida.setText(new StringBuilder(modeloFood.getNombre()));
        txt_descComida.setText(new StringBuilder(modeloFood.getDescripcion()));
        txt_precioComida.setText(new StringBuilder(String.valueOf(modeloFood.getPrecio())));

        ((AppCompatActivity) getActivity()).getSupportActionBar()
                .setTitle(Common.comidaSelec.getNombre());
        //tamaño
        for(ModeloSize modeloSize: Common.comidaSelec.getTamano()) {
            RadioButton radioButton = new RadioButton(getContext());
            radioButton.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b)
                    Common.comidaSelec.setUsuariosSelectedTamano(modeloSize);

                calculateTotalPrecio();
            });

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f);
            radioButton.setLayoutParams(params);
            radioButton.setText(modeloSize.getNombre());
            radioButton.setTag(modeloSize.getPrecio());
            radio.addView(radioButton);
        }
        if(radio.getChildCount()>0){
            RadioButton radioButton = (RadioButton) radio.getChildAt(0);
            radioButton.setChecked(true);
        }
        calculateTotalPrecio();
    }
    private void calculateTotalPrecio(){
        double totalPrecio = Double.parseDouble(String.valueOf(Common.comidaSelec.getPrecio())), displayPrecio=0.0;
        totalPrecio += Double.parseDouble(String.valueOf(Common.comidaSelec.getUsuariosSelectedTamano().getPrecio()));
        displayPrecio = totalPrecio * (Integer.parseInt(number_button.getNumber()));
        displayPrecio = Math.round(displayPrecio*100.0/100.0);

        txt_precioComida.setText(new StringBuilder("").append(Common.formatPrecio(displayPrecio)).toString());

    }

    @Override
    public void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }
}
