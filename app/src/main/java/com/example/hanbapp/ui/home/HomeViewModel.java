package com.example.hanbapp.ui.home;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.hanbapp.Callback.ICategoriaCallbackListener;
import com.example.hanbapp.Callback.IPopularCallbackListener;
import com.example.hanbapp.Callback.IRestaurantCallbackListener;
import com.example.hanbapp.Common.Common;
import com.example.hanbapp.Model.ModeloCategorias;
import com.example.hanbapp.Model.ModeloLoMasPopular;
import com.example.hanbapp.Model.ModeloRestaurant;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HomeViewModel extends ViewModel implements ICategoriaCallbackListener, IPopularCallbackListener, IRestaurantCallbackListener {

    private MutableLiveData<List<ModeloCategorias>> categoriaList;
    private MutableLiveData<List<ModeloLoMasPopular>> popularList;
    private MutableLiveData<List<ModeloRestaurant>> restaurantesList;
    private MutableLiveData<String> mensajeError;
    private ICategoriaCallbackListener categoriaCallbackListener;
    private IPopularCallbackListener popularCallbackListener;
    private IRestaurantCallbackListener restaurantCallbackListener;

    public HomeViewModel() {
        categoriaCallbackListener = this;
        restaurantCallbackListener = this;
        popularCallbackListener = this;
    }

    public MutableLiveData<List<ModeloLoMasPopular>> getPopularList() {
        if (popularList == null) {
            popularList = new MutableLiveData<>();
            mensajeError = new MutableLiveData<>();
            loadPopularList();
        }
        return popularList;
    }

    private void loadPopularList() {
        List<ModeloLoMasPopular> tempList = new ArrayList<>();
        DatabaseReference popularRef = FirebaseDatabase.getInstance().getReference(Common.POPULAR_REFERENCES);
        popularRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot itemSnapshot:dataSnapshot.getChildren()) {
                    ModeloLoMasPopular modelo = itemSnapshot.getValue(ModeloLoMasPopular.class);
                    tempList.add(modelo);
                }
                popularCallbackListener.onPopularLoadSuccess(tempList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                popularCallbackListener.onPopularLoadFailed(databaseError.getMessage());
            }
        });
    }

    public MutableLiveData<List<ModeloCategorias>> getCategoriaList() {
        if (categoriaList == null) {
            categoriaList = new MutableLiveData<>();
            mensajeError = new MutableLiveData<>();
            loadCategoriesList();
        }
        return categoriaList;
    }

    private void loadCategoriesList() {
        List<ModeloCategorias> tempList = new ArrayList<>();
        DatabaseReference categoriaRef = FirebaseDatabase.getInstance().getReference(Common.CATEGORY_REFERENCES);
        categoriaRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot itemSnapshot:dataSnapshot.getChildren()) {
                    ModeloCategorias modelo = itemSnapshot.getValue(ModeloCategorias.class);
                    tempList.add(modelo);
                }
                categoriaCallbackListener.onCategoryLoadSuccess(tempList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                categoriaCallbackListener.onCategoryLoadFailed(databaseError.getMessage());
            }
        });
    }

    public MutableLiveData<List<ModeloRestaurant>> getRestaurantesList() {
        if (restaurantesList == null) {
            restaurantesList = new MutableLiveData<>();
            mensajeError = new MutableLiveData<>();
            loadRestaurantesList();
        }
        return restaurantesList;
    }

    private void loadRestaurantesList() {
        List<ModeloRestaurant> tempList = new ArrayList<>();
        DatabaseReference restaurantRef = FirebaseDatabase.getInstance().getReference(Common.RESTAURANT_REFERENCES);
        restaurantRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot itemSnapshot:dataSnapshot.getChildren()) {
                    ModeloRestaurant modelo = itemSnapshot.getValue(ModeloRestaurant.class);
                    tempList.add(modelo);
                }
                restaurantCallbackListener.onRestaurantLoadSuccess(tempList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                restaurantCallbackListener.onRestaurantLoadFailed(databaseError.getMessage());
            }
        });
    }

    public MutableLiveData<String> getMensajeError() {
        return mensajeError;
    }

    @Override
    public void onCategoryLoadSuccess(List<ModeloCategorias> modeloCategorias) {
        categoriaList.setValue(modeloCategorias);
    }

    @Override
    public void onCategoryLoadFailed(String mensaje) {
        mensajeError.setValue(mensaje);
    }

    @Override
    public void onPopularLoadSuccess(List<ModeloLoMasPopular> modeloLosMasPopulares) {
        popularList.setValue(modeloLosMasPopulares);
    }

    @Override
    public void onPopularLoadFailed(String mensaje) {
        mensajeError.setValue(mensaje);
    }

    @Override
    public void onRestaurantLoadSuccess(List<ModeloRestaurant> modelosRestaurantes) {
        restaurantesList.setValue(modelosRestaurantes);
    }

    @Override
    public void onRestaurantLoadFailed(String mensaje) {
        mensajeError.setValue(mensaje);
    }
}