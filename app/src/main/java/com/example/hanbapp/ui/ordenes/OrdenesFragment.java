package com.example.hanbapp.ui.ordenes;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hanbapp.Adaptadores.AdaptadorOrdenes;
import com.example.hanbapp.Callback.ILoadOrderCallbackListener;
import com.example.hanbapp.Common.Common;
import com.example.hanbapp.Model.ModeloOrden;
import com.example.hanbapp.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dmax.dialog.SpotsDialog;

public class OrdenesFragment extends Fragment implements ILoadOrderCallbackListener {
    private OrdenesViewModel ordenesViewModel;
    Unbinder unbinder;
    AlertDialog dialog;

    @BindView(R.id.recycler_ordenes)
    RecyclerView recycler_ordenes;

    private ILoadOrderCallbackListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ordenesViewModel = ViewModelProviders.of(this).get(OrdenesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_ordenes, container, false);
        unbinder = ButterKnife.bind(this, root);
        initView(root);
        loadOrdenesDesdeFirebase();
        ordenesViewModel.getOrdenesList().observe(getViewLifecycleOwner(),ordenList -> {
            AdaptadorOrdenes adaptador = new AdaptadorOrdenes(getContext(),ordenList);
            recycler_ordenes.setAdapter(adaptador);
        });
        return root;
    }

    private void loadOrdenesDesdeFirebase() {
        List<ModeloOrden> ordenList = new ArrayList<>();
        FirebaseDatabase.getInstance().getReference(Common.ORDEN_REF)
                .orderByChild("idUsuario")
                .equalTo(Common.currentUser.getTelefono())
                .limitToLast(100)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot ordenSnapshot: dataSnapshot.getChildren())
                        {
                            ModeloOrden orden = ordenSnapshot.getValue(ModeloOrden.class);
                            orden.setNumeroOrden(ordenSnapshot.getKey());
                            ordenList.add(orden);
                        }
                        listener.onLoadOrdenSuccess(ordenList);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        listener.onLoadOrdenFailed(databaseError.getMessage());
                    }
                });
    }

    private void initView(View root) {
        listener = this;
        dialog = new SpotsDialog.Builder().setCancelable(false).setContext(getContext()).build();

        recycler_ordenes.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_ordenes.setLayoutManager(layoutManager);
        recycler_ordenes.addItemDecoration(new DividerItemDecoration(getContext(),layoutManager.getOrientation()));
    }


    @Override
    public void onLoadOrdenSuccess(List<ModeloOrden> ordenList) {
        dialog.dismiss();
        ordenesViewModel.setOrdenesList(ordenList);
    }

    @Override
    public void onLoadOrdenFailed(String mensaje) {
        dialog.dismiss();
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }
}
