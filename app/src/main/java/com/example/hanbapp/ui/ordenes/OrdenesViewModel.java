package com.example.hanbapp.ui.ordenes;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.hanbapp.Model.ModeloOrden;

import java.util.List;

public class OrdenesViewModel extends ViewModel {
    private MutableLiveData<List<ModeloOrden>> ordenesList;

    public OrdenesViewModel() {
        ordenesList = new MutableLiveData<>();
    }

    public MutableLiveData<List<ModeloOrden>> getOrdenesList() {
        return ordenesList;
    }

    public void setOrdenesList(List<ModeloOrden> ordenesList) {
        this.ordenesList.setValue(ordenesList);
    }
}
