package com.example.hanbapp.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asksira.loopingviewpager.LoopingViewPager;
import com.example.hanbapp.Adaptadores.AdaptadorCategorias;
import com.example.hanbapp.Adaptadores.AdaptadorPopular;
import com.example.hanbapp.Adaptadores.AdaptadorRestaurant;
import com.example.hanbapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeFragment extends Fragment {
    private HomeViewModel homeViewModel;
    Unbinder unbinder;

    @BindView(R.id.recycler_categorias)
    RecyclerView recycler_categorias;
    @BindView(R.id.recycler_restaurant)
    RecyclerView recycler_restaurant;
    //temporal, es el banner
    //@BindView(R.id.viewpager)
    //LoopingViewPager viewpager;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, root);
        init();

        homeViewModel.getCategoriaList().observe(getViewLifecycleOwner(), modeloCategorias -> {
            //Adaptador
            AdaptadorCategorias adaptador = new AdaptadorCategorias(getContext(), modeloCategorias);
            recycler_categorias.setAdapter(adaptador);
        });

        /* temporal, es el banner
        homeViewModel.getPopularList().observe(getViewLifecycleOwner(), modeloLoMasPopulars -> {
            //Adaptador
            AdaptadorPopular adaptador = new AdaptadorPopular(getContext(), modeloLoMasPopulars, true);
            viewpager.setAdapter(adaptador);
        });*/

        homeViewModel.getRestaurantesList().observe(getViewLifecycleOwner(), modeloRestaurants -> {
            //Adaptador
            AdaptadorRestaurant adaptador = new AdaptadorRestaurant(getContext(), modeloRestaurants);
            recycler_restaurant.setAdapter(adaptador);
        });

        return root;
    }

    public void init() {
        recycler_categorias.setHasFixedSize(true);
        recycler_categorias.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        recycler_restaurant.setHasFixedSize(true);
        recycler_restaurant.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
    }

    /*temporal, es para el banner
    @Override
    public void onResume() {
        super.onResume();
        viewpager.resumeAutoScroll();
    }

    @Override
    public void onPause() {
        viewpager.pauseAutoScroll();
        super.onPause();
    }*/
}
