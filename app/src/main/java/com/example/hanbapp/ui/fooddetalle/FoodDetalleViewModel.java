package com.example.hanbapp.ui.fooddetalle;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.hanbapp.Common.Common;
import com.example.hanbapp.Model.ModeloFood;

public class FoodDetalleViewModel extends ViewModel {
    private MutableLiveData<ModeloFood> detalleComidaList;

    public FoodDetalleViewModel() {
    }

    public MutableLiveData<ModeloFood> getDetalleComidaList() {
        if (detalleComidaList == null)
                detalleComidaList = new MutableLiveData<>();
        detalleComidaList.setValue(Common.comidaSelec);
        return detalleComidaList;
    }
}