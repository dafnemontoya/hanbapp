package com.example.hanbapp.ui.foodlist;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.hanbapp.Common.Common;
import com.example.hanbapp.Model.ModeloFood;

import java.util.List;

public class FoodListViewModel extends ViewModel {
    private MutableLiveData<List<ModeloFood>> foodList;

    public FoodListViewModel() {
    }

    public MutableLiveData<List<ModeloFood>> getFoodList() {
        if (foodList == null)
            foodList = new MutableLiveData<>();
        foodList.setValue(Common.categoriaSelec.getComidas());
        return foodList;
    }
}
