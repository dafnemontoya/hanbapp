package com.example.hanbapp.ui.carrito;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.hanbapp.Common.Common;
import com.example.hanbapp.Database.CarritoDataSource;
import com.example.hanbapp.Database.CarritoDatabase;
import com.example.hanbapp.Database.CarritoLocalDataSource;
import com.example.hanbapp.Database.ItemCarrito;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CarritoViewModel extends ViewModel {
    private CompositeDisposable compositeDisposable;
    private CarritoDataSource carritoDataSource;
    private MutableLiveData<List<ItemCarrito>> mutableLiveDataCarritoItems;

    public CarritoViewModel() {
        compositeDisposable = new CompositeDisposable();
    }

    public void initCarritoDataSource(Context context) {
        carritoDataSource = new CarritoLocalDataSource(CarritoDatabase.getInstancia(context).carritoDAO());
    }

    public void onStop() {
        compositeDisposable.clear();
    }

    public MutableLiveData<List<ItemCarrito>> getMutableLiveDataCarritoItems() {
        if (mutableLiveDataCarritoItems == null)
            mutableLiveDataCarritoItems = new MutableLiveData<>();
        getAllCarritoItems();
        return mutableLiveDataCarritoItems;
    }

    private void getAllCarritoItems() {
        compositeDisposable.add(carritoDataSource.getAllCarrito(Common.currentUser.getTelefono())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itemCarritos -> {
                            mutableLiveDataCarritoItems.setValue(itemCarritos);
                        }, throwable -> {
                            mutableLiveDataCarritoItems.setValue(null);
                        }
                ));
    }


}