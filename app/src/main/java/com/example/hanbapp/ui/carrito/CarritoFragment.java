package com.example.hanbapp.ui.carrito;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hanbapp.Adaptadores.AdaptadorCarrito;
import com.example.hanbapp.Callback.ILoadTimeFromFirebaseListener;
import com.example.hanbapp.Common.Common;
import com.example.hanbapp.Common.MySwipeHelper;
import com.example.hanbapp.Database.CarritoDataSource;
import com.example.hanbapp.Database.CarritoDatabase;
import com.example.hanbapp.Database.CarritoLocalDataSource;
import com.example.hanbapp.Database.ItemCarrito;
import com.example.hanbapp.EventBus.ContadorCarritoEvento;
import com.example.hanbapp.EventBus.HideCarritoFAB;
import com.example.hanbapp.EventBus.UpdateItemCarrito;
import com.example.hanbapp.Model.ModeloOrden;
import com.example.hanbapp.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CarritoFragment extends Fragment implements ILoadTimeFromFirebaseListener {
    private CarritoViewModel carritoViewModel;
    private CarritoDataSource carritoDataSource;
    private Parcelable estadoRecyclerView;
    private Unbinder unbinder;
    private AdaptadorCarrito adaptador;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    ILoadTimeFromFirebaseListener listener;

    @BindView(R.id.recycler_carrito)
    RecyclerView recycler_carrito;
    @BindView(R.id.txtPrecioTotal)
    TextView txtPrecioTotal;
    @BindView(R.id.txtCarritoVacio)
    TextView txtCarritoVacio;
    @BindView(R.id.group_place_holder)
    CardView group_place_holder;

    @OnClick(R.id.btnConfOrden)
    void onConfirmarOrdenClick() {
        pagoCOD(); //COD quiere decir Cash On Delivery, es decir, Envío contra Reembolso
    }

    private void pagoCOD() {
        compositeDisposable.add(carritoDataSource.getAllCarrito(Common.currentUser.getTelefono())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itemCarritos -> {
                    //Cuando tengamos todos los items del carrito, sacaremos el precio total
                    carritoDataSource.sumaPreciosInCarrito(Common.currentUser.getTelefono())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new SingleObserver<Double>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onSuccess(Double precioTotal) {
                                    double pagoFinal = precioTotal;
                                    ModeloOrden orden = new ModeloOrden();
                                    orden.setIdUsuario(Common.currentUser.getTelefono());
                                    orden.setNombreUsuario(Common.currentUser.getNombre());
                                    //orden.setComentario(comentario);
                                    orden.setListaItemsCarrito(itemCarritos);
                                    orden.setPagoTotal(precioTotal);
                                    orden.setDescuento(0); //quizas después se implementen códigos de descuento
                                    orden.setPagoFinal(pagoFinal);
                                    orden.setCod(true);
                                    orden.setIdTransaccion("Envío contra reembolso (COD)");

                                    //Enviar a Firebase
                                    //enviarOrdenAFirebase(orden);
                                    sincronizarTiempoLocalConTiempoGlobal(orden);
                                }

                                @Override
                                public void onError(Throwable e) {
                                    //Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                }, throwable -> {
                    Toast.makeText(getContext(), ""+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }));
    }

    private void sincronizarTiempoLocalConTiempoGlobal(ModeloOrden orden) {
        final DatabaseReference offsetRef = FirebaseDatabase.getInstance().getReference(".info/serverTimeOffset");
        offsetRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                long offset = dataSnapshot.getValue(Long.class);
                long tiempoEstimadoServerMs = System.currentTimeMillis()+offset; //offset es el tiempo perdido entre el tiempo local y el tiempo global (servidor)
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy HH:mm");
                Date resultDate = new Date(tiempoEstimadoServerMs);
                Log.d("TEST_FECHA",""+sdf.format(resultDate));

                listener.onLoadTimeSuccess(orden,tiempoEstimadoServerMs);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onLoadTimeFailed(databaseError.getMessage());
            }
        });
    }

    private void enviarOrdenAFirebase(ModeloOrden orden) {
        FirebaseDatabase.getInstance().getReference(Common.ORDEN_REF)
                .child(Common.crearNumOrden()) //Genera un número único para cada orden
                .setValue(orden)
                .addOnFailureListener(e -> {
                    Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }).addOnCompleteListener(task -> {
                    //En caso de éxito :D
                    carritoDataSource.cleanCarrito(Common.currentUser.getTelefono())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new SingleObserver<Integer>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onSuccess(Integer integer) {
                                    //ÉXITO :)
                                    Toast.makeText(getContext(), "¡Tu orden se ha enviado de manera exitosa!", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onError(Throwable e) {//dudoso
                                    Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                });
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        carritoViewModel =
                ViewModelProviders.of(this).get(CarritoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_carrito, container, false);
        listener = this;
        carritoViewModel.initCarritoDataSource(getContext());
        carritoViewModel.getMutableLiveDataCarritoItems().observe(getViewLifecycleOwner(), new Observer<List<ItemCarrito>>() {
            @Override
            public void onChanged(List<ItemCarrito> itemCarritos) {
                if (itemCarritos == null || itemCarritos.isEmpty()) {
                    recycler_carrito.setVisibility(View.GONE);
                    group_place_holder.setVisibility(View.GONE);
                    txtCarritoVacio.setVisibility(View.VISIBLE);
                }
                else {
                    recycler_carrito.setVisibility(View.VISIBLE);
                    group_place_holder.setVisibility(View.VISIBLE);
                    txtCarritoVacio.setVisibility(View.GONE);

                    adaptador = new AdaptadorCarrito(getContext(), itemCarritos);
                    recycler_carrito.setAdapter(adaptador);
                }
            }
        });
        unbinder = ButterKnife.bind(this, root);
        initViews();
        return root;
    }

    private void initViews() {
        setHasOptionsMenu(true);
        carritoDataSource = new CarritoLocalDataSource(CarritoDatabase.getInstancia(getContext()).carritoDAO());

        EventBus.getDefault().postSticky(new HideCarritoFAB(true));

        recycler_carrito.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycler_carrito.setLayoutManager(layoutManager);
        recycler_carrito.addItemDecoration(new DividerItemDecoration(getContext(), layoutManager.getOrientation()));

        //Esto es para borrar un item del carrito :V
        MySwipeHelper mySwipeHelper = new MySwipeHelper(getContext(),recycler_carrito,200) {
            @Override
            public void instantiateMyButton(RecyclerView.ViewHolder viewHolder, List<MyButton> buffer) {
                buffer.add(new MyButton(getContext(),"Eliminar", 30,0, Color.parseColor("#FF3C30"),
                        pos -> {
                            ItemCarrito itemCarrito = adaptador.getItemAtPosition(pos);
                            carritoDataSource.deleteItemCarrito(itemCarrito)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new SingleObserver<Integer>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onSuccess(Integer integer) {
                                            adaptador.notifyItemRemoved(pos);
                                            sumarTodosLosItemsEnCarrito();
                                            EventBus.getDefault().postSticky(new ContadorCarritoEvento(true)); //Actualizar ícono flotante
                                            Toast.makeText(getContext(), "Se ha eliminado del carrito exitosamente", Toast.LENGTH_SHORT).show();
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }));
            }
        };

        sumarTodosLosItemsEnCarrito();
    }

    private void sumarTodosLosItemsEnCarrito() {
        carritoDataSource.sumaPreciosInCarrito(Common.currentUser.getTelefono())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Double>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Double aDouble) {
                        txtPrecioTotal.setText(new StringBuilder("Total: $").append(aDouble));
                    }

                    @Override
                    public void onError(Throwable e) {
                        //if (!e.getMessage().contains("Query returned empty"))
                            //Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        menu.findItem(R.id.action_settings).setVisible(false); //esconder menú settings
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_carrito, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_limpiar_carrito) {
            carritoDataSource.cleanCarrito(Common.currentUser.getTelefono())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Integer>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(Integer integer) {
                            Toast.makeText(getContext(), "Carrito limpio", Toast.LENGTH_SHORT).show();
                            EventBus.getDefault().postSticky(new ContadorCarritoEvento(true));
                        }

                        @Override
                        public void onError(Throwable e) {//dudoso
                            Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().postSticky(new HideCarritoFAB(false));
        carritoViewModel.onStop();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        compositeDisposable.clear();
        super.onStop();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onUpdateItemInCarritoEvent(UpdateItemCarrito event) {
        if (event.getItemCarrito() != null)
        {
            //Guardar estado del recycler view
            estadoRecyclerView = recycler_carrito.getLayoutManager().onSaveInstanceState();
            carritoDataSource.updateItemCarrito(event.getItemCarrito())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Integer>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(Integer integer) {
                            calcularPrecioTotal();
                            recycler_carrito.getLayoutManager().onRestoreInstanceState(estadoRecyclerView); //Fix error refresh recycler view after update
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(getContext(), "(ACTUALIZAR CARRITO)"+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void calcularPrecioTotal() {
        carritoDataSource.sumaPreciosInCarrito(Common.currentUser.getTelefono())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Double>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Double precio) {
                        txtPrecioTotal.setText(new StringBuilder("Total: $")
                            .append(Common.formatPrecio(precio)));
                    }

                    @Override
                    public void onError(Throwable e) {
                        //Toast.makeText(getContext(), "(SUMA ITEMS CARRITO)"+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    @Override
    public void onLoadTimeSuccess(ModeloOrden orden, long tiempoEstimadoEnMs) {
        orden.setFechaCreacion(tiempoEstimadoEnMs);
        orden.setStatus(1);
        enviarOrdenAFirebase(orden);
    }

    @Override
    public void onLoadTimeFailed(String mensaje) {
        Toast.makeText(getContext(), ""+mensaje, Toast.LENGTH_SHORT).show();
    }
}
