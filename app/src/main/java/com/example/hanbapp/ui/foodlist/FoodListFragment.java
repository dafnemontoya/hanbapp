package com.example.hanbapp.ui.foodlist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hanbapp.Adaptadores.AdaptadorFoodList;
import com.example.hanbapp.Common.Common;
import com.example.hanbapp.Model.ModeloFood;
import com.example.hanbapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FoodListFragment extends Fragment {
    private FoodListViewModel foodListViewModel;
    Unbinder unbinder;

    @BindView(R.id.recycler_food_list)
    RecyclerView recycler_food_list;

    AdaptadorFoodList adaptador;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        foodListViewModel = ViewModelProviders.of(this).get(FoodListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_list_food, container, false);
        unbinder = ButterKnife.bind(this, root);
        initViews();
        foodListViewModel.getFoodList().observe(getViewLifecycleOwner(), new Observer<List<ModeloFood>>() {
            @Override
            public void onChanged(List<ModeloFood> modeloFoods) {
                adaptador = new AdaptadorFoodList(getContext(),modeloFoods);
                recycler_food_list.setAdapter(adaptador);
            }
        });
        return root;
    }

    private void initViews() {
        ((AppCompatActivity) getActivity()).getSupportActionBar()
                .setTitle(Common.categoriaSelec.getNombre());

        recycler_food_list.setHasFixedSize(true);
        recycler_food_list.setLayoutManager(new LinearLayoutManager(getContext()));
    }


}
