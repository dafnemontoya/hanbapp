package com.example.hanbapp.Database;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public interface CarritoDataSource {
    Flowable<List<ItemCarrito>> getAllCarrito(String uid);

    Single<Integer> countItemInCarrito(String uid);

    Single<Double> sumaPreciosInCarrito(String uid);

    Single<ItemCarrito> getItemInCarrito(String idComida, String uid);

    Single<ItemCarrito> getItemConTodasLasOpcionesEnCarrito(String idComida ,String uid, String tamanoComida, String complementoComida);

    Completable insertOrReplaceAll(ItemCarrito... itemsCarrito);

    Single<Integer> updateItemCarrito(ItemCarrito itemCarrito);

    Single<Integer> deleteItemCarrito(ItemCarrito itemCarrito);

    Single<Integer> cleanCarrito(String uid);
}
