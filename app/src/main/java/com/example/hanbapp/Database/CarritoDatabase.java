package com.example.hanbapp.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(version = 1, entities = ItemCarrito.class, exportSchema = false)
public abstract class CarritoDatabase extends RoomDatabase {
    public abstract CarritoDAO carritoDAO();
    private static CarritoDatabase instancia;

    public static CarritoDatabase getInstancia(Context context) {
        if (instancia == null)
            instancia = Room.databaseBuilder(context, CarritoDatabase.class, "HanbappDB2").build();
        return instancia;
    }
}
