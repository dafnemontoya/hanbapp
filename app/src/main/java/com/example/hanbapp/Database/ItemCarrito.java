package com.example.hanbapp.Database;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Carrito", primaryKeys = {"uid","idComida","tamanoComida","complementoComida"})
public class ItemCarrito {
    @NonNull
    @ColumnInfo(name = "idComida")
    private String idComida;

    @ColumnInfo(name = "nombreComida")
    private String nombreComida;

    @ColumnInfo(name = "imagenComida")
    private String imagenComida;

    @ColumnInfo(name = "precioComida")
    private Double precioComida;

    @ColumnInfo(name = "cantidadComida")
    private int cantidadComida;

    /*@ColumnInfo(name = "telUsuario")
    private String telUsuario;*/

    @ColumnInfo(name = "extraPrecioComida")
    private Double extraPrecioComida;

    @NonNull
    @ColumnInfo(name = "complementoComida")
    private String complementoComida;

    @NonNull
    @ColumnInfo(name = "tamanoComida")
    private String tamanoComida;

    @NonNull
    @ColumnInfo(name = "uid") //uid será el teléfono celular
    private String uid;

    @NonNull
    public String getIdComida() {
        return idComida;
    }

    public void setIdComida(@NonNull String idComida) {
        this.idComida = idComida;
    }

    public String getNombreComida() {
        return nombreComida;
    }

    public void setNombreComida(String nombreComida) {
        this.nombreComida = nombreComida;
    }

    public String getImagenComida() {
        return imagenComida;
    }

    public void setImagenComida(String imagenComida) {
        this.imagenComida = imagenComida;
    }

    public Double getPrecioComida() {
        return precioComida;
    }

    public void setPrecioComida(Double precioComida) {
        this.precioComida = precioComida;
    }

    public int getCantidadComida() {
        return cantidadComida;
    }

    public void setCantidadComida(int cantidadComida) {
        this.cantidadComida = cantidadComida;
    }

    public Double getExtraPrecioComida() {
        return extraPrecioComida;
    }

    public void setExtraPrecioComida(Double extraPrecioComida) {
        this.extraPrecioComida = extraPrecioComida;
    }

    public String getComplementoComida() {
        return complementoComida;
    }

    public void setComplementoComida(String complementoComida) {
        this.complementoComida = complementoComida;
    }

    public String getTamanoComida() {
        return tamanoComida;
    }

    public void setTamanoComida(String tamanoComida) {
        this.tamanoComida = tamanoComida;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof ItemCarrito))
            return false;
        ItemCarrito itemCarrito = (ItemCarrito) obj;
        return  itemCarrito.getIdComida().equals(this.idComida) &&
                itemCarrito.getTamanoComida().equals(this.tamanoComida) &&
                itemCarrito.getComplementoComida().equals(this.complementoComida);

    }
}
