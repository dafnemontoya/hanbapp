package com.example.hanbapp.Database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface CarritoDAO {
    @Query("SELECT * FROM Carrito WHERE uid=:uid")
    Flowable<List<ItemCarrito>> getAllCarrito(String uid);

    @Query("SELECT SUM(cantidadComida) FROM Carrito WHERE uid=:uid")
    Single<Integer> countItemInCarrito(String uid);

    @Query("SELECT SUM((precioComida + extraPrecioComida)  * cantidadComida) FROM Carrito WHERE uid=:uid")
    Single<Double> sumaPreciosInCarrito(String uid);

    @Query("SELECT * FROM Carrito WHERE idComida=:idComida AND uid=:uid")
    Single<ItemCarrito> getItemInCarrito(String idComida, String uid);

    @Query("SELECT * FROM Carrito WHERE idComida=:idComida AND uid=:uid AND tamanoComida=:tamanoComida AND complementoComida=:complementoComida")
    Single<ItemCarrito> getItemConTodasLasOpcionesEnCarrito(String idComida ,String uid, String tamanoComida, String complementoComida);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertOrReplaceAll(ItemCarrito... itemsCarrito);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    Single<Integer> updateItemCarrito(ItemCarrito itemCarrito);

    @Delete
    Single<Integer> deleteItemCarrito(ItemCarrito itemCarrito);

    @Query("DELETE FROM Carrito WHERE uid=:uid")
    Single<Integer> cleanCarrito(String uid);
}
