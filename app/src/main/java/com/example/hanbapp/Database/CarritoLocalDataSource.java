package com.example.hanbapp.Database;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class CarritoLocalDataSource implements CarritoDataSource {
    private CarritoDAO carritoDAO;

    public CarritoLocalDataSource(CarritoDAO carritoDAO) {
        this.carritoDAO = carritoDAO;
    }

    @Override
    public Flowable<List<ItemCarrito>> getAllCarrito(String uid) {
        return carritoDAO.getAllCarrito(uid);
    }

    @Override
    public Single<Integer> countItemInCarrito(String uid) {
        return carritoDAO.countItemInCarrito(uid);
    }

    @Override
    public Single<Double> sumaPreciosInCarrito(String uid) {
        return carritoDAO.sumaPreciosInCarrito(uid);
    }

    @Override
    public Single<ItemCarrito> getItemInCarrito(String idComida, String uid) {
        return carritoDAO.getItemInCarrito(idComida,uid);
    }

    @Override
    public Single<ItemCarrito> getItemConTodasLasOpcionesEnCarrito(String idComida, String uid, String tamanoComida, String complementoComida) {
        return carritoDAO.getItemConTodasLasOpcionesEnCarrito(idComida,uid,tamanoComida,complementoComida);
    }

    @Override
    public Completable insertOrReplaceAll(ItemCarrito... itemsCarrito) {
        return carritoDAO.insertOrReplaceAll(itemsCarrito);
    }

    @Override
    public Single<Integer> updateItemCarrito(ItemCarrito itemCarrito) {
        return carritoDAO.updateItemCarrito(itemCarrito);
    }

    @Override
    public Single<Integer> deleteItemCarrito(ItemCarrito itemCarrito) {
        return carritoDAO.deleteItemCarrito(itemCarrito);
    }

    @Override
    public Single<Integer> cleanCarrito(String uid) {
        return carritoDAO.cleanCarrito(uid);
    }
}
