package com.example.hanbapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.hanbapp.Model.Usuarios;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegistroActivity extends AppCompatActivity {
    EditText edtCel_signup, edtUsuario_signup, edtPass_signup;
    Button btnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        edtUsuario_signup = (EditText) findViewById(R.id.edtUsuario_signup);
        edtPass_signup = (EditText) findViewById(R.id.edtPass_signup);
        edtCel_signup = (EditText) findViewById(R.id.edtCel_signup);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);

        //Iniciar Firebase
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference tabla_usuarios = database.getReference("Usuarios"); //nombre de la "tabla" usuarios en Firebase

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtUsuario_signup.getText().toString().isEmpty() || edtCel_signup.getText().toString().isEmpty() ||
                        edtPass_signup.getText().toString().isEmpty()) {
                    Toast.makeText(RegistroActivity.this, "Favor de rellenar todos los campos", Toast.LENGTH_SHORT).show();
                } else {
                    //dialogo de espera
                    final ProgressDialog mDialog = new ProgressDialog(RegistroActivity.this);
                    mDialog.setMessage("Por favor, espere un momento...");
                    mDialog.show();

                    tabla_usuarios.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            // Revisa si ya se registró el número de teléfono en la Base de Datos
                            if (dataSnapshot.child(edtCel_signup.getText().toString()).exists()) {
                                mDialog.dismiss();
                                Toast.makeText(RegistroActivity.this, "Este teléfono ya está registrado", Toast.LENGTH_SHORT).show();
                            } else {
                                mDialog.dismiss();
                                Usuarios usuario = new Usuarios(edtUsuario_signup.getText().toString(), edtPass_signup.getText().toString(), edtCel_signup.getText().toString());
                                tabla_usuarios.child(edtCel_signup.getText().toString()).setValue(usuario);
                                Toast.makeText(RegistroActivity.this, "¡Registro exitoso! :)", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });
    } //FIN onCreate


}
