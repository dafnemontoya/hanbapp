package com.example.hanbapp.Callback;

import com.example.hanbapp.Model.ModeloOrden;

public interface ILoadTimeFromFirebaseListener {
    void onLoadTimeSuccess(ModeloOrden orden, long tiempoEstimadoEnMs);
    void onLoadTimeFailed(String mensaje);
}
