package com.example.hanbapp.Callback;

import com.example.hanbapp.Model.ModeloRestaurant;

import java.util.List;

public interface IRestaurantCallbackListener {
    void onRestaurantLoadSuccess(List<ModeloRestaurant> modelosRestaurantes);
    void onRestaurantLoadFailed(String mensaje);
}
