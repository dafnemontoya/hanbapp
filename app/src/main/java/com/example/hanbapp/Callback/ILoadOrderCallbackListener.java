package com.example.hanbapp.Callback;

import com.example.hanbapp.Model.ModeloOrden;

import java.util.List;

public interface ILoadOrderCallbackListener {
    void onLoadOrdenSuccess(List<ModeloOrden> ordenList);
    void onLoadOrdenFailed(String mensaje);
}
