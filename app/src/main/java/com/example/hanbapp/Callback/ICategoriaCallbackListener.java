package com.example.hanbapp.Callback;

import com.example.hanbapp.Model.ModeloCategorias;

import java.util.List;

public interface ICategoriaCallbackListener {
    void onCategoryLoadSuccess(List<ModeloCategorias> modelosCategorias);
    void onCategoryLoadFailed(String mensaje);
}
