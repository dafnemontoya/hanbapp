package com.example.hanbapp.Callback;

import com.example.hanbapp.Model.ModeloLoMasPopular;

import java.util.List;

public interface IPopularCallbackListener {
    void onPopularLoadSuccess(List<ModeloLoMasPopular> modeloLosMasPopulares);
    void onPopularLoadFailed(String mensaje);
}
