package com.example.hanbapp.Callback;

import android.view.View;

public interface IRecyclerClickListener {
    void onItemClickListener(View view, int pos);
}
