package com.example.hanbapp.Model;

public class ModeloRestaurant {
    private String Imagen, Nombre, Descripcion;

    public ModeloRestaurant() {
    }

    public ModeloRestaurant(String imagen, String nombre, String descripcion) {
        Imagen = imagen;
        Nombre = nombre;
        Descripcion = descripcion;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String imagen) {
        Imagen = imagen;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
