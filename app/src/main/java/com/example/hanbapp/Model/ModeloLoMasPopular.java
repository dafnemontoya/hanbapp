package com.example.hanbapp.Model;

public class ModeloLoMasPopular {
    private String idMenu, idComida, nombre, imagen;

    public ModeloLoMasPopular() {
    }

    public ModeloLoMasPopular(String idMenu, String idComida, String nombre, String imagen) {
        this.idMenu = idMenu;
        this.idComida = idComida;
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(String idMenu) {
        this.idMenu = idMenu;
    }

    public String getIdComida() {
        return idComida;
    }

    public void setIdComida(String idComida) {
        this.idComida = idComida;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
