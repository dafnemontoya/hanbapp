package com.example.hanbapp.Model;

public class ModeloSize {
    private String Nombre;
    private double Precio;

    public ModeloSize() {
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double precio) {
        Precio = precio;
    }

}
