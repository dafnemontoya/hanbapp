package com.example.hanbapp.Model;

import java.util.List;

public class ModeloCategorias {
    private String menu_id, Nombre, Imagen;
    List<ModeloFood> Comidas; //foods

    public ModeloCategorias() {
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String imagen) {
        Imagen = imagen;
    }

    public List<ModeloFood> getComidas() {
        return Comidas;
    }

    public void setComidas(List<ModeloFood> comidas) {
        Comidas = comidas;
    }
}
