package com.example.hanbapp.Model;

import java.util.List;

public class ModeloFood {
    private String Id,Nombre,Imagen,Descripcion;
    private long Precio;
    private List<ModeloComplementos> Complementos;
    private List<ModeloSize> Tamano;
    private Double ratingValue;
    private long ratingCount;
    private List<ModeloComplementos> usuariosSelectedComplementos;
    private ModeloSize usuariosSelectedTamano;

    public ModeloFood() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String imagen) {
        Imagen = imagen;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public long getPrecio() {
        return Precio;
    }

    public void setPrecio(long precio) {
        Precio = precio;
    }

    public List<ModeloComplementos> getComplementos() {
        return Complementos;
    }

    public void setComplementos(List<ModeloComplementos> complementos) {
        Complementos = complementos;
    }

    public List<ModeloSize> getTamano() {
        return Tamano;
    }

    public void setTamano(List<ModeloSize> tamano) {
        Tamano = tamano;
    }

    public Double getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(Double ratingValue) {
        this.ratingValue = ratingValue;
    }

    public long getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(long ratingCount) {
        this.ratingCount = ratingCount;
    }

    public List<ModeloComplementos> getUsuariosSelectedComplementos() {
        return usuariosSelectedComplementos;
    }

    public void setUsuariosSelectedComplementos(List<ModeloComplementos> usuariosSelectedComplementos) {
        this.usuariosSelectedComplementos = usuariosSelectedComplementos;
    }

    public ModeloSize getUsuariosSelectedTamano() {
        return usuariosSelectedTamano;
    }

    public void setUsuariosSelectedTamano(ModeloSize usuariosSelectedTamano) {
        this.usuariosSelectedTamano = usuariosSelectedTamano;
    }
}
