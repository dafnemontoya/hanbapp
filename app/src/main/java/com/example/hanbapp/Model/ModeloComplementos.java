package com.example.hanbapp.Model;

public class ModeloComplementos {
    private String Nombre;
    private long Precio;

    public ModeloComplementos() {
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public long getPrecio() {
        return Precio;
    }

    public void setPrecio(long precio) {
        Precio = precio;
    }
}
