package com.example.hanbapp.Model;

import com.example.hanbapp.Database.ItemCarrito;

import java.util.List;

public class ModeloOrden {
    private String IdUsuario, NombreUsuario, comentario, IdTransaccion;
    private double lst, lng, PagoTotal, PagoFinal;
    private boolean cod;
    private int Descuento;
    private List<ItemCarrito> ListaItemsCarrito;
    private long fechaCreacion;
    private String numeroOrden;
    private int status;

    public ModeloOrden() {
    }

    public String getIdUsuario() { //idUsuario es su telefono
        return IdUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        IdUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return NombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        NombreUsuario = nombreUsuario;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getIdTransaccion() {
        return IdTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        IdTransaccion = idTransaccion;
    }

    public double getLst() {
        return lst;
    }

    public void setLst(double lst) {
        this.lst = lst;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getPagoTotal() {
        return PagoTotal;
    }

    public void setPagoTotal(double pagoTotal) {
        PagoTotal = pagoTotal;
    }

    public double getPagoFinal() {
        return PagoFinal;
    }

    public void setPagoFinal(double pagoFinal) {
        PagoFinal = pagoFinal;
    }

    public boolean isCod() {
        return cod;
    }

    public void setCod(boolean cod) {
        this.cod = cod;
    }

    public int getDescuento() {
        return Descuento;
    }

    public void setDescuento(int descuento) {
        Descuento = descuento;
    }

    public List<ItemCarrito> getListaItemsCarrito() {
        return ListaItemsCarrito;
    }

    public void setListaItemsCarrito(List<ItemCarrito> listaItemsCarrito) {
        this.ListaItemsCarrito = listaItemsCarrito;
    }

    public long getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(long fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
