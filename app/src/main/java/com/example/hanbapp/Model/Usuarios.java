package com.example.hanbapp.Model;

public class Usuarios {
    private String Nombre, Password, Telefono;

    public Usuarios() {
    }

    public Usuarios(String nombre, String password, String telefono) {
        Nombre = nombre;
        Password = password;
        Telefono = telefono;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }
}
