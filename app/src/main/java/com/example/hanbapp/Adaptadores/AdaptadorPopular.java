package com.example.hanbapp.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.asksira.loopingviewpager.LoopingPagerAdapter;
import com.bumptech.glide.Glide;
import com.example.hanbapp.Model.ModeloLoMasPopular;
import com.example.hanbapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AdaptadorPopular extends LoopingPagerAdapter<ModeloLoMasPopular> {

    @BindView(R.id.imgPopular)
    ImageView imgPopular;
    @BindView(R.id.txtPopular)
    TextView txtPopular;

    Unbinder unbinder;

    public AdaptadorPopular(Context context, List<ModeloLoMasPopular> itemList, boolean isInfinite) {
        super(context, itemList, isInfinite);
    }

    @Override
    protected View inflateView(int viewType, ViewGroup container, int listPosition) {
        return LayoutInflater.from(context).inflate(R.layout.layout_popular_item, container, false);
    }

    @Override
    protected void bindView(View convertView, int listPosition, int viewType) {
        unbinder = ButterKnife.bind(this, convertView);
        //Asignar datos
        Glide.with(convertView).load(itemList.get(listPosition).getImagen()).into(imgPopular);
        txtPopular.setText(itemList.get(listPosition).getNombre());
    }
}
