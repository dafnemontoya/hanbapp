package com.example.hanbapp.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hanbapp.Common.Common;
import com.example.hanbapp.Model.ModeloOrden;
import com.example.hanbapp.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AdaptadorOrdenes extends RecyclerView.Adapter<AdaptadorOrdenes.MyViewHolder>  {
    private Context context;
    private List<ModeloOrden> ordenesList;
    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;

    public AdaptadorOrdenes(Context context, List<ModeloOrden> ordenesList) {
        this.context = context;
        this.ordenesList = ordenesList;
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    }



    @NonNull
    @Override
    public AdaptadorOrdenes.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_orden_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(ordenesList.get(position).getListaItemsCarrito().get(0).getImagenComida())
                .into(holder.imgOrden);
        calendar.setTimeInMillis(ordenesList.get(position).getFechaCreacion());
        Date date = new Date(ordenesList.get(position).getFechaCreacion());
        holder.txtFechaOrden.setText(new StringBuilder(Common.getDateOfWeek(calendar.get(Calendar.DAY_OF_WEEK))).append(" ")
            .append(simpleDateFormat.format(date)));
        holder.txtNumOrden.setText(new StringBuilder("Número de orden: ")
            .append(ordenesList.get(position).getNumeroOrden()));
        holder.txtStatus.setText(new StringBuilder("Status: ").append(Common.convertirStatusATexto(ordenesList.get(position).getStatus())));
    }

    @Override
    public int getItemCount() {
        return ordenesList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        Unbinder unbinder;

        @BindView(R.id.imgOrden)
        ImageView imgOrden;
        @BindView(R.id.txtFechaOrden)
        TextView txtFechaOrden;
        @BindView(R.id.txtNumOrden)
        TextView txtNumOrden;
        @BindView(R.id.txtStatus)
        TextView txtStatus;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }
}
