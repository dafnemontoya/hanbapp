package com.example.hanbapp.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hanbapp.Callback.IRecyclerClickListener;
import com.example.hanbapp.Common.Common;
import com.example.hanbapp.EventBus.CategoriaClick;
import com.example.hanbapp.Model.ModeloCategorias;
import com.example.hanbapp.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdaptadorCategorias extends RecyclerView.Adapter<AdaptadorCategorias.MyViewHolder> {
    Context context;
    List<ModeloCategorias> modeloCategorias;

    public AdaptadorCategorias(Context context, List<ModeloCategorias> modeloCategorias) {
        this.context = context;
        this.modeloCategorias = modeloCategorias;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.layout_categorias_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(modeloCategorias.get(position).getImagen())
                .into(holder.imgCategoria);
        holder.txt_nomCategoria.setText(modeloCategorias.get(position).getNombre());

        //Evento
        holder.setListener(new IRecyclerClickListener() {
            @Override
            public void onItemClickListener(View view, int pos) {
                Common.categoriaSelec = modeloCategorias.get(pos);
                EventBus.getDefault().postSticky(new CategoriaClick(true, modeloCategorias.get(pos)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return modeloCategorias.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Unbinder unbinder;

        @BindView(R.id.txt_nomCategoria)
        TextView txt_nomCategoria;
        @BindView(R.id.imgCategoria)
        CircleImageView imgCategoria;

        IRecyclerClickListener listener;

        public void setListener(IRecyclerClickListener listener) {
            this.listener = listener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClickListener(v, getAdapterPosition());
        }
    }
}
