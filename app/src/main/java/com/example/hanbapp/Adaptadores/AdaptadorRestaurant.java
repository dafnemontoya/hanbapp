package com.example.hanbapp.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hanbapp.Model.ModeloRestaurant;
import com.example.hanbapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AdaptadorRestaurant extends RecyclerView.Adapter<AdaptadorRestaurant.MyViewHolder> {
    Context context;
    List<ModeloRestaurant> modeloRestaurant;

    public AdaptadorRestaurant(Context context, List<ModeloRestaurant> modeloRestaurant) {
        this.context = context;
        this.modeloRestaurant = modeloRestaurant;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.layout_restaurant_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(modeloRestaurant.get(position).getImagen())
                .into(holder.imgRest);
        holder.txtNomRest.setText(modeloRestaurant.get(position).getNombre());
        holder.txtDescRestaurant.setText(modeloRestaurant.get(position).getDescripcion());
    }

    @Override
    public int getItemCount() {
        return modeloRestaurant.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        Unbinder unbinder;

        @BindView(R.id.imgRest)
        ImageView imgRest;
        @BindView(R.id.txtNomRest)
        TextView txtNomRest;
        @BindView(R.id.txtDescRestaurant)
        TextView txtDescRestaurant;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }

}
