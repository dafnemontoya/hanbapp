package com.example.hanbapp.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.hanbapp.Database.ItemCarrito;
import com.example.hanbapp.EventBus.UpdateItemCarrito;
import com.example.hanbapp.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AdaptadorCarrito extends RecyclerView.Adapter<AdaptadorCarrito.MyViewHolder> {
    Context context;
    List<ItemCarrito> itemCarritoList;

    public AdaptadorCarrito(Context context, List<ItemCarrito> itemCarritoList) {
        this.context = context;
        this.itemCarritoList = itemCarritoList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_carrito_item, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(itemCarritoList.get(position).getImagenComida()).into(holder.img_carrito);
        holder.txt_nomPlatillo.setText(new StringBuilder(itemCarritoList.get(position).getNombreComida()));
        holder.txt_precioPlatillo.setText(new StringBuilder("$ ")
            .append(itemCarritoList.get(position).getPrecioComida() + itemCarritoList.get(position).getExtraPrecioComida()));

        //number button
        holder.numberButton.setNumber(String.valueOf(itemCarritoList.get(position).getCantidadComida()));
        holder.numberButton.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                //cuando el usuario presione el botón, se actualizará la bd
                itemCarritoList.get(position).setCantidadComida(newValue);
                EventBus.getDefault().postSticky(new UpdateItemCarrito(itemCarritoList.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemCarritoList.size();
    }

    public ItemCarrito getItemAtPosition(int pos) {
        return itemCarritoList.get(pos);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private Unbinder unbinder;

        @BindView(R.id.img_carrito)
        ImageView img_carrito;
        @BindView(R.id.txt_nomPlatillo)
        TextView txt_nomPlatillo;
        @BindView(R.id.txt_precioPlatillo)
        TextView txt_precioPlatillo;
        @BindView(R.id.number_button)
        ElegantNumberButton numberButton;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }
}
