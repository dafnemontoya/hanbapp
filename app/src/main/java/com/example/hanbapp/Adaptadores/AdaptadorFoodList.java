package com.example.hanbapp.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.hanbapp.Callback.IRecyclerClickListener;
import com.example.hanbapp.Common.Common;
import com.example.hanbapp.EventBus.FoodItemClick;
import com.example.hanbapp.Model.ModeloFood;
import com.example.hanbapp.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class AdaptadorFoodList extends RecyclerView.Adapter<AdaptadorFoodList.MyViewHolder>  {
    private Context context;
    private List<ModeloFood> foodModelList;
    //private CompositeDisposable compositeDisposable;
    //private CarritoDataSource carritoDataSource;

    public AdaptadorFoodList(Context context, List<ModeloFood> foodModelList) {
        this.context = context;
        this.foodModelList = foodModelList;
        //this.compositeDisposable = new CompositeDisposable();
        //this.carritoDataSource = new CarritoLocalDataSource(CarritoDatabase.getInstancia(context).carritoDAO());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.layout_food_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(foodModelList.get(position).getImagen())
                .into(holder.imgComida);
        holder.txtNomComida.setText(foodModelList.get(position).getNombre());
        holder.txtPrecioComida.setText(new StringBuilder("$")
                .append(foodModelList.get(position).getPrecio()));

        //evento
        holder.setListener((view, pos) -> {
            Common.comidaSelec = foodModelList.get(pos);
            EventBus.getDefault().postSticky(new FoodItemClick(true, foodModelList.get(pos)));
        });


        /* agregar al carrito de manera rápida
        holder.img_quick_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemCarrito itemCarrito = new ItemCarrito();
                itemCarrito.setUid(Common.currentUser.getTelefono()); //recordar que es el teléfono

                itemCarrito.setIdComida(foodModelList.get(position).getId());
                itemCarrito.setNombreComida(foodModelList.get(position).getNombre());
                itemCarrito.setImagenComida(foodModelList.get(position).getImagen());
                itemCarrito.setCantidadComida(1);
                itemCarrito.setPrecioComida(Double.valueOf(String.valueOf(foodModelList.get(position).getPrecio())));
                itemCarrito.setExtraPrecioComida(0.0);
                itemCarrito.setTamanoComida("Default");
                itemCarrito.setComplementoComida("Default");

                carritoDataSource.getItemConTodasLasOpcionesEnCarrito(
                        itemCarrito.getIdComida(),
                        Common.currentUser.getTelefono(),
                        itemCarrito.getTamanoComida(),
                        itemCarrito.getComplementoComida())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<ItemCarrito>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onSuccess(ItemCarrito itemCarritoFromDB) {
                                if (itemCarritoFromDB.equals(itemCarrito)) {
                                    //Ya en la DB, sólo hay que actualizar
                                    itemCarritoFromDB.setExtraPrecioComida(itemCarrito.getExtraPrecioComida());
                                    itemCarritoFromDB.setComplementoComida(itemCarrito.getComplementoComida());
                                    itemCarritoFromDB.setTamanoComida(itemCarrito.getTamanoComida());
                                    itemCarritoFromDB.setCantidadComida(itemCarritoFromDB.getCantidadComida() + itemCarrito.getCantidadComida());

                                    carritoDataSource.updateItemCarrito(itemCarritoFromDB)
                                    .subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new SingleObserver<Integer>() {
                                                @Override
                                                public void onSubscribe(Disposable d) {

                                                }

                                                @Override
                                                public void onSuccess(Integer integer) {
                                                    Toast.makeText(context, "Se actualizó el carrito correctamente :)", Toast.LENGTH_SHORT).show();
                                                    EventBus.getDefault().postSticky(new ContadorCarritoEvento(true));
                                                }

                                                @Override
                                                public void onError(Throwable e) {
                                                    Toast.makeText(context, "(ACTUALIZAR CARRITO)"+e.getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                }
                                else {
                                    //El item no estaba en el carrito antes, es decir, se inserta uno nuevo
                                    compositeDisposable.add(carritoDataSource.insertOrReplaceAll(itemCarrito)
                                            .subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(()->{
                                                Toast.makeText(context, "Se agregó al carrito existosamente :)", Toast.LENGTH_SHORT).show(); 
                                                }, throwable -> {
                                                Toast.makeText(context, "(ERROR DEL CARRITO)"+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                            }));
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                if (e.getMessage().contains("empty")) {
                                    //Default, si el carrito está vacío
                                    compositeDisposable.add(carritoDataSource.insertOrReplaceAll(itemCarrito)
                                            .subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(()->{
                                                Toast.makeText(context, "Se agregó al carrito existosamente :)", Toast.LENGTH_SHORT).show();
                                            }, throwable -> {
                                                Toast.makeText(context, "(ERROR DEL CARRITO)"+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                            }));
                                }
                                else
                                    Toast.makeText(context, "(OBTENER CARRITO)"+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return foodModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Unbinder unbinder;

        @BindView(R.id.imgComida)
        ImageView imgComida;
        @BindView(R.id.txtNomComida)
        TextView txtNomComida;
        @BindView(R.id.txtPrecioComida)
        TextView txtPrecioComida;
        @BindView(R.id.img_quick_cart)
        ImageView img_quick_cart;

        IRecyclerClickListener listener;

        public void setListener(IRecyclerClickListener listener) {
            this.listener = listener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClickListener(v, getAdapterPosition());
        }
    }
}
