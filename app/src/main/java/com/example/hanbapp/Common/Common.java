package com.example.hanbapp.Common;

import com.example.hanbapp.Model.ModeloCategorias;
import com.example.hanbapp.Model.ModeloComplementos;
import com.example.hanbapp.Model.ModeloFood;
import com.example.hanbapp.Model.ModeloSize;
import com.example.hanbapp.Model.Usuarios;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;

public class Common {
    //Así se llaman las "tablas" en la BD de Firebase
    public static final String POPULAR_REFERENCES = "MasPopular";
    public static final String CATEGORY_REFERENCES = "Categoria";
    public static final String RESTAURANT_REFERENCES = "Restaurant";
    public static final String USER_REFERENCES = "Usuarios";
    public static final int DEFAULT_COLUMN_COUNT = 0;
    public static final int FULL_WIDTH_COLUMN = 1;
    public static final String ORDEN_REF = "Ordenes";

    public static Usuarios currentUser;
    public static ModeloCategorias categoriaSelec;
    public static ModeloFood comidaSelec;

    public static String formatPrecio(double precio) {
        if (precio != 0) {
            DecimalFormat df = new DecimalFormat("#,##0.00");
            df.setRoundingMode(RoundingMode.UP);
            String finalPrecio = new StringBuilder(df.format(precio)).toString();
            return finalPrecio.replace(".",",");
        }else
            return "0,00";
    }

    public static Double calcularPrecioExtra(ModeloSize usuariosSelectedTamano, List<ModeloComplementos> usuariosSelectedComplementos) {
        Double resultado = 0.0;
        if (usuariosSelectedTamano == null && usuariosSelectedComplementos == null)
            return 0.0;
        else if(usuariosSelectedTamano == null) {
            //Si el usuario sólo agregó complementos
            for(ModeloComplementos modeloComplementos : usuariosSelectedComplementos)
                resultado+=modeloComplementos.getPrecio();
            return resultado;
        }
        else if(usuariosSelectedComplementos == null) {
            //Si el usuario no seleccionó ningún complemento
            return usuariosSelectedTamano.getPrecio()*1.0;
        }
        else {
            //Si el usuario agrega tanto complemento y tamaño
            resultado = usuariosSelectedTamano.getPrecio()*1.0;
            for (ModeloComplementos modeloComplementos : usuariosSelectedComplementos)
                resultado+=modeloComplementos.getPrecio();
            return resultado;
        }
    }

    public static String crearNumOrden() {
        return new StringBuilder()
                .append(System.currentTimeMillis()) //Hora actual en milisegundos
                .append(Math.abs(new Random().nextInt())) //Generar un número aleatorio para evitar repeticiones
                .toString();
    }

    public static String getDateOfWeek(int i) {
        switch (i)
        {
            case 1:
                return "Domingo";
            case 2:
                return "Lunes";
            case 3:
                return "Martes";
            case 4:
                return "Miércoles";
            case 5:
                return "Jueves";
            case 6:
                return "Viernes";
            case 7:
                return "Sábado";
            default:
                return "Desconocido";
        }
    }

    public static String convertirStatusATexto(int status) {
        switch (status) {
            case 1:
                return "Procesado";
            case 2:
                return "Enviando";
            case 3:
                return "Enviado";
            case 4:
                return "Cancelado";
            default:
                return "Desconocido";
        }
    }


}
